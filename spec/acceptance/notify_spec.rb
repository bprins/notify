require 'spec_helper_acceptance'

log_file = '/var/log/puppetlabs/agent.log'

pp_basic_usage = <<-PUPPETCODE
  include notify
PUPPETCODE

pp_custom_message = <<-PUPPETCODE
  class { notify:
    message => 'hello litmus',
  }
PUPPETCODE

describe 'notify' do
  context 'when default message is applied' do
    it do
      apply_manifest(pp_basic_usage)

      describe file(log_file) do
        it { is_expected.to contain 'Just an arbitrary message' }
      end
    end
  end

  context 'when a custom message is applied' do
    it do
      apply_manifest(pp_custom_message)

      describe file(log_file) do
        it { is_expected.to contain 'hello litmus' }
      end
    end
  end
end
