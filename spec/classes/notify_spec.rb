# frozen_string_literal: true

require 'spec_helper'

describe 'notify' do
  it { is_expected.to contain_notify('agent log notification') }

  context 'with message => hello' do
    let(:params) { { 'message' => 'hello' } }

    it { is_expected.to contain_notify('agent log notification').with_message('hello') }
  end
end
